/**
 * @file
 * Context Taxonomy Term page module admin helpers.
 */

/*global jQuery, Drupal*/

(function ($) {
  "use strict";

  Drupal.behaviors.context_taxonomy_page = {
    attach: function (context) {
      // Target taxonomy term screen.
      $('.context-plugin-forms .context-plugin-form-taxonomy_term').once('context-plugin-form-taxonomy_term', function () {
        var activeForm = $(this);
        // Enable/disable term selects on enabling/disabling of vocab
        // checkboxes.
        $('.form-item-conditions-plugins-taxonomy-term-values-vocabs').find('input[type="checkbox"]').change(function () {
          var $select = activeForm.find('.form-item-conditions-plugins-taxonomy-term-values-terms-vocab-' + $(this).val().replace('_', '-') + ' select');
          if ($(this).is(':checked')) {
            $select.attr('disabled', 'disabled');
          }
          else {
            $select.removeAttr('disabled');
          }
        }).trigger('change');
      });
    }
  };
})(jQuery);
