<?php
/**
 * @file
 * Context condition plugin for taxonomy term pages.
 *
 * This plugin is based on extending existing taxonomy term plugin.
 */

/**
 * Context condition plugin for taxonomy term pages.
 */
class context_taxonomy_page_condition_term_page extends context_condition_taxonomy_term {
  private $reset = TRUE;

  /**
   * {@inheritdoc}
   */
  function condition_values() {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function condition_form($context) {
    // Retrieve saved form values.
    $defaults = $this->fetch_from_context($context, 'values');

    // Since we are reusing existing taxonomy term plugin, we are reusing it's
    // storage as well. This means that any string values are considered
    // to be vocab names and numeric are term ids. It is safe to store them in
    // a single array since all terms have unique term ids across all vocabs.
    // single array.
    $defaults_vocab = array();
    $defaults_terms = array();
    foreach ($defaults as $k => $v) {
      // Skip any unset values.
      if ($v === 0) {
        continue;
      }
      if (is_numeric($k)) {
        $defaults_terms[$k] = $v;
      }
      else {
        $defaults_vocab[$k] = $v;
      }
    }

    // Reuse existing vocabs element and alter its label.
    $form['vocabs'] = parent::condition_form($context);
    $form['vocabs']['#title'] = t('Taxonomy vocabularies');
    $form['vocabs']['#options'] = parent::condition_values();
    $form['vocabs']['#default_value'] = $defaults_vocab;
    $form['vocabs']['#description'] = t('Select vocabulary to include all current and future terms from this vocabulary or select specific terms below.');

    // Prepare hierarchical list of terms.
    $count = 0;
    $vocabs = taxonomy_get_vocabularies();
    foreach ($vocabs as $vid => $vocab) {
      $tree = taxonomy_get_tree($vid);
      if ($tree && (count($tree) > 0)) {
        $options = array();
        foreach ($tree as $term) {
          $options[$term->tid] = str_repeat('-', $term->depth) . $term->name;
        }

        $form['terms']['vocab-' . $vocab->machine_name] = array(
          '#prefix' => $count === 0 ? '<div class="select-group-wrapper">' : '',
          '#suffix' => $count === count($vocabs) - 1 ? '</div>' : '',
          '#title' => check_plain($vocab->name),
          '#type' => 'select',
          '#size' => 12,
          '#multiple' => TRUE,
          '#options' => $options,
          '#default_value' => $defaults_terms,
        );
      }
      $count++;
    }

    $form['divider'] = array(
      '#markup' => '<div class="description clearfix">' . t('Select specific terms to apply context to.') . '</div>',
    );

    drupal_add_css(drupal_get_path('module', 'context_taxonomy_page') . '/plugins/context_taxonomy_page.css');
    drupal_add_js(drupal_get_path('module', 'context_taxonomy_page') . '/plugins/context_taxonomy_page.js');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function condition_form_submit($values) {
    // This submit handler is called twice and we are skipping it for the first
    // time as it does not contain all values.
    if (!isset($values['vocabs']) || !isset($values['terms'])) {
      return $values;
    }

    // Get all selected vocabs.
    $vocabs = array();
    if (isset($values['vocabs'])) {
      foreach ($values['vocabs'] as $vocab_name => $checked) {
        if ($checked) {
          $vocabs[$vocab_name] = $checked;
        }
      }
    }

    // Get terms.
    $all_terms = array();
    foreach ($values['terms'] as $vocab_name => $terms) {
      if (empty($terms)) {
        continue;
      }
      $all_terms = $all_terms + array_combine($terms, $terms);

      $vocab_name = substr($vocab_name, strlen('vocab-'));
      // Remove any vocabs that have their terms set.
      if (array_key_exists($vocab_name, $vocabs)) {
        unset($vocabs[$vocab_name]);
      }
    }

    return $vocabs + $all_terms;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form($context) {
    $defaults = $this->fetch_from_context($context, 'options');

    $form = array();
    $form['include_children'] = array(
      '#title' => t('Include children'),
      '#description' => t('Include current and future children terms of selected terms.'),
      '#type' => 'checkbox',
      '#default_value' => isset($defaults['include_children']) ? $defaults['include_children'] : FALSE,
    );

    $form['exclude_selected'] = array(
      '#title' => t('Exclude selected terms'),
      '#description' => t('Exclude selected terms to include only children terms.'),
      '#type' => 'checkbox',
      '#default_value' => isset($defaults['exclude_selected']) ? $defaults['exclude_selected'] : FALSE,
      '#states' => array(
        'visible' => array(
          ':input[name="conditions[plugins][taxonomy_term][options][include_children]"]' => array('checked' => TRUE),
        ),
      ),
    );

    return $form + parent::options_form($context);;
  }

  /**
   * {@inheritdoc}
   */
  function options_form_submit($values) {
    // Disable exclude_selected if include children is not checked.
    if ($values['exclude_selected'] && !$values['include_children']) {
      $values['exclude_selected'] = "0";
    }

    return parent::options_form_submit($values);
  }

  /**
   * {@inheritdoc}
   */
  public function execute($term, $op) {
    // Match terms on the forms and normal view using custom function.
    $is_form_values = array(1, 2);
    $contexts = $this->get_contexts();
    foreach ($contexts as $context) {
      // @todo: Consider adding caching of matched contexts.
      if ($op === 'form') {
        $options = $this->fetch_from_context($context, 'options');
        if (!empty($options['term_form']) && in_array($options['term_form'], $is_form_values)) {
          $this->match_term($context, $term);
        }
      }
      elseif (empty($options['term_form']) || $options['term_form'] != 2) {
        $this->match_term($context, $term);
      }
    }

    // Match vocabularies using existing parent class.
    parent::execute($term, $op);
  }

  /**
   * Match context to a provided term.
   *
   * Also, handle any children terms, if set in context options.
   *
   * @param array $context
   *   Context array to use values from for matching.
   * @param object $term
   *   Term object to match to.
   *
   * @return array
   *   Array of matched contexts names.
   */
  protected function match_term($context, $term) {
    $values = $this->fetch_from_context($context, 'values');
    $matched = array();
    foreach ($values as $value) {
      // Use only term ids.
      if (is_numeric($value)) {
        $terms = array();
        $options = $this->fetch_from_context($context, 'options');

        // Include term itself.
        $terms[$term->tid] = $term->tid;

        // Add all parents for current term to scan through.
        if (isset($options['include_children']) && $options['include_children']) {
          $parent_terms = taxonomy_get_parents_all($term->tid);
          foreach ($parent_terms as $parent_term) {
            $terms[$parent_term->tid] = $parent_term->tid;
          }
        }

        // Exclude term itself if it has been excluded in options.
        if ($options['exclude_selected']) {
          unset($terms[$term->tid]);
        }

        if (in_array($value, $terms)) {
          $this->condition_met($context, $terms[$value]);
          $matched[] = $context->name;
        }
      }
    }

    return $matched;
  }
}
