<?php
/**
 * @file
 * Provides context condition for Context Taxonomy Term Page.
 *
 * This module is based on extending existing taxonomy term plugin.
 *
 * How to use:
 * - To set context for all terms in vocabulary, check vocabulary checkbox.
 *   This behavior is derived directly from Taxonomy Term plugin shipped with
 *   Context module.
 *
 * - To set context for a term in a vocabulary, select vocabulary and
 *   a term. If vocabulary is not selected, it will be selected automatically
 *   on form submit.
 *
 * - To set context for multiple terms across multiple vocabs, select
 *   multiple terms across multiple vocabs If vocabulary is not selected,
 *   it will be selected automatically on form submit.
 *
 * - To set context for a term and all it's children, select vocabulary, term
 *   and option 'Include term children'. If vocabulary is not selected, it will
 *   be selected automatically on form submit.
 */

/**
 * Implementation of hook_context_plugins().
 */
function context_taxonomy_page_context_plugins() {
  $plugins['context_taxonomy_page_condition_term_page'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_taxonomy_page') . '/plugins',
      'file' => 'context_taxonomy_page_condition_term_page.inc',
      'class' => 'context_taxonomy_page_condition_term_page',
      'parent' => 'context_condition_taxonomy_term',
    ),
  );

  return $plugins;
}

/**
 * Implementation of hook_context_registry_alter().
 */
function context_taxonomy_page_context_registry_alter(&$registry) {
  if (isset($registry['conditions']['taxonomy_term'])) {
    $registry['conditions']['taxonomy_term']['plugin'] = 'context_taxonomy_page_condition_term_page';
  }
}

/**
 * Implementation of hook_form_alter().
 */
function context_taxonomy_page_form_alter(&$form, $form_state, $form_id) {
  if ($form_id === 'taxonomy_form_term'){
    // Trigger the condition in an after_build function to avoid being skipped
    // when there are validation errors.
    $form['#after_build'][] = 'context_taxonomy_page_form_alter_term_after_build';
  }
}

/**
 * Taxonomy term form after build callback.
 */
function context_taxonomy_page_form_alter_term_after_build($form, &$form_state) {
  // Prevent this from firing on admin pages.
  if ($form['#form_id'] == 'taxonomy_form_term' && arg(0) != 'admin') {
    if ($plugin = context_get_plugin('condition', 'taxonomy_term')) {
      // Covert term array to object to pass to plugin.
      $term = json_decode (json_encode ($form['#term']), FALSE);
      $plugin->execute($term, 'form');
    }
  }

  return $form;
}
